<?php

namespace App\Controller;


use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Date;

class AnnonceController extends AbstractController
{
    /**
     * @Route("/annonce", name="annonce")
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function annonce(Request $request)
    {
        $user = $this->getUser();
        $username = $user->getUsername();
        $numtel = $user->getNumtel();
        $genre = $user->getGenre();

        $choicesVoitures = array();

        $client = HttpClient::create();
        $response = $client->request('GET', 'http://127.0.0.1:8001/voiturebyuser/' . $user->getId());
        foreach ($response->toArray() as $value) {
            array_push($choicesVoitures, [$value["type"]["nom"] . " " . $value["type"]["modelevoiture"]["modele"] => $value["id"]]);
        }


        if (empty($username) || empty($numtel) || empty($genre) || empty($choicesVoitures)) {

            $this->addFlash('success', 'Veuillez completez votre profil!');
            return $this->redirectToRoute('profil');
        }
        $form = $this->createFormBuilder()
            ->add('Villedepart', ChoiceType::class, ['choices' => [

                'Tunis' => 'Tunis',
                'Sousse' => 'Sousse',
                'Nabeul' => 'Nabeul',
                'Sfax' => 'Sfax',
                'Monastir' => 'Monastir',
                'Mahdia' => 'Mahdia',
                ' Kairouan' => ' Kairouan',
                'Zaghouan' => 'Zaghouan',
                ' Bizerte' => ' Bizerte',
                'Ben Arous' => 'Ben Arous',
                'Manouba' => 'Manouba',
                'Ariana' => 'Ariana',
                'Gabès' => 'Gabès',
                'Médenine' => 'Médenine',
                'Tataouine' => 'Tataouine',
                ' Kébili' => ' Kébili',
                'Tozeur' => 'Tozeur',
                ' Gafsa' => ' Gafsa',
                'Sidi Bouzid' => 'Sidi Bouzid',
                'Kasserine' => 'Kasserine',
                'Siliana' => 'Siliana',
                'Kef' => 'Kef',
                'Jendouba' => 'Jendouba',
                ' Béja' => ' Béja',

            ],])
            ->add('villearrivee', ChoiceType::class, ['choices' => [

                'Sousse' => 'Sousse',
                'Nabeul' => 'Nabeul',
                'Sfax' => 'Sfax',
                'Monastir' => 'Monastir',
                'Mahdia' => 'Mahdia',
                ' Kairouan' => ' Kairouan',
                'Zaghouan' => 'Zaghouan',
                ' Bizerte' => ' Bizerte',
                'Ben Arous' => 'Ben Arous',
                'Manouba' => 'Manouba',
                'Ariana' => 'Ariana',
                'Tunis' => 'Tunis',
                'Gabès' => 'Gabès',
                'Médenine' => 'Médenine',
                'Tataouine' => 'Tataouine',
                ' Kébili' => ' Kébili',
                'Tozeur' => 'Tozeur',
                ' Gafsa' => ' Gafsa',
                'Sidi Bouzid' => 'Sidi Bouzid',
                'Kasserine' => 'Kasserine',
                'Siliana' => 'Siliana',
                'Kef' => 'Kef',
                'Jendouba' => 'Jendouba',
                ' Béja' => ' Béja',

            ],])
            ->add('voiture', ChoiceType::class, ['choices' => $choicesVoitures])
            ->add('pointarrivee', TextType::class)
            ->add('pointdepart', TextType::class)
            ->add('datedepart', DateType::class, [
                'widget' => 'single_text',
                'data' => new \DateTime(),
                'format' => 'yyyy-MM-dd',
                'attr' => ['class' => 'js-datepicker'],])
            ->add('heuredepart', TimeType::class, [
                'input' => 'datetime',
                'widget' => 'single_text'
            ])
            ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
                $form = $event->getForm();
                $form->get('datedepart')->setData($form->get("datedepart")->getViewData());
                // $form->get('heuredepart')->setData($form->get("heuredepart")->getViewData());

            })
            ->add('prix', MoneyType::class, [
                'currency' => "TND",
            ])
            ->add('musique', CheckBoxType::class, ['required' => false,])
            ->add('pause', CheckBoxType::class, ['required' => false,])
            ->add('bagage', CheckBoxType::class, ['required' => false,])
            ->add('createdby', HiddenType::class, [
                'data' => $user->getUsername(),
            ])
            ->add('submit', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $x = $form->getData();
            $x["datedepart"] = date_format($x["datedepart"], "yy-m-d");
            $x["heuredepart"] = date_format($x["heuredepart"], "H:i");
            $client = HttpClient::create();
            $response = $client->request('POST', 'http://127.0.0.1:8001/postuler', [
                'headers' => ["Content-Type" => "application/json"],
                'body' => json_encode($x),
            ]);
            return $this->redirectToRoute('accueil');

        }

        return $this->render(
            'annonce/annonce.html.twig',
            array('form' => $form->createView())
        );
    }

    /**
     * @Route("/detailannonce/{annonceID}", name="detailannonce")
     *
     */
    public function detailannonce(Request $request, int $annonceID)
    {
        $client = HttpClient::create();
        $response = $client->request('GET', 'http://127.0.0.1:8001/detail/' . $annonceID);
        //dd($response->getContent());
        return $this->render(
            'annonce/detailannonce.html.twig'
            , ["data" => $response->toArray()]

        );
    }


    /**
     * @Route("/modifierAnnonce/{annonceID}", name="modificationAnannonce")
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function modifierAnnonce(Request $request, int $annonceID)
    {
        $user = $this->getUser();
        $username = $user->getUsername();
        $numtel = $user->getNumtel();
        $genre = $user->getGenre();

        $choicesVoitures = array();

        $client = HttpClient::create();
        $response = $client->request('GET', 'http://127.0.0.1:8001/voiturebyuser/' . $user->getId());
        $currentAnnonce = $client->request('GET', 'http://127.0.0.1:8001/detail/' . $annonceID)->toArray();

        foreach ($response->toArray() as $value) {
            array_push($choicesVoitures, [$value["type"]["nom"] . " " . $value["type"]["modelevoiture"]["modele"] => $value["id"]]);
        }


        if (empty($username) || empty($numtel) || empty($genre) || empty($choicesVoitures)) {

            $this->addFlash('success', 'Veuillez completez votre profil!');
            return $this->redirectToRoute('profil');
        }
        $form = $this->createFormBuilder()
            ->add('Villedepart', ChoiceType::class, ['choices' => [

                'Tunis' => 'Tunis',
                'Sousse' => 'Sousse',
                'Nabeul' => 'Nabeul',
                'Sfax' => 'Sfax',
                'Monastir' => 'Monastir',
                'Mahdia' => 'Mahdia',
                ' Kairouan' => ' Kairouan',
                'Zaghouan' => 'Zaghouan',
                ' Bizerte' => ' Bizerte',
                'Ben Arous' => 'Ben Arous',
                'Manouba' => 'Manouba',
                'Ariana' => 'Ariana',
                'Gabès' => 'Gabès',
                'Médenine' => 'Médenine',
                'Tataouine' => 'Tataouine',
                ' Kébili' => ' Kébili',
                'Tozeur' => 'Tozeur',
                ' Gafsa' => ' Gafsa',
                'Sidi Bouzid' => 'Sidi Bouzid',
                'Kasserine' => 'Kasserine',
                'Siliana' => 'Siliana',
                'Kef' => 'Kef',
                'Jendouba' => 'Jendouba',
                ' Béja' => ' Béja',

            ], 'data' => $currentAnnonce['trajet']['villedepart']
            ])
            ->add('villearrivee', ChoiceType::class, ['choices' => [

                'Sousse' => 'Sousse',
                'Nabeul' => 'Nabeul',
                'Sfax' => 'Sfax',
                'Monastir' => 'Monastir',
                'Mahdia' => 'Mahdia',
                ' Kairouan' => ' Kairouan',
                'Zaghouan' => 'Zaghouan',
                ' Bizerte' => ' Bizerte',
                'Ben Arous' => 'Ben Arous',
                'Manouba' => 'Manouba',
                'Ariana' => 'Ariana',
                'Tunis' => 'Tunis',
                'Gabès' => 'Gabès',
                'Médenine' => 'Médenine',
                'Tataouine' => 'Tataouine',
                ' Kébili' => ' Kébili',
                'Tozeur' => 'Tozeur',
                ' Gafsa' => ' Gafsa',
                'Sidi Bouzid' => 'Sidi Bouzid',
                'Kasserine' => 'Kasserine',
                'Siliana' => 'Siliana',
                'Kef' => 'Kef',
                'Jendouba' => 'Jendouba',
                ' Béja' => ' Béja',

            ], 'data' => $currentAnnonce['trajet']['villearrivee']])
            ->add('voiture', ChoiceType::class, ['choices' => $choicesVoitures])
            ->add('pointarrivee', TextType::class, ['data' => $currentAnnonce['pointarrivee']])
            ->add('pointdepart', TextType::class, ['data' => $currentAnnonce['pointdepart']])
            ->add('datedepart', DateType::class, [
                'widget' => 'single_text',
                'data' => new \DateTime($currentAnnonce['datedepart']),
                'format' => 'yyyy-MM-dd',
                'attr' => ['class' => 'js-datepicker'],])
            ->add('heuredepart', TimeType::class, [
                'input' => 'datetime',
                'widget' => 'single_text',
                'data' => new \DateTime($currentAnnonce['heuredepart']),

            ])
            ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
                $form = $event->getForm();
                $form->get('datedepart')->setData($form->get("datedepart")->getViewData());
                // $form->get('heuredepart')->setData($form->get("heuredepart")->getViewData());

            })
            ->add('prix', MoneyType::class, [
                'currency' => "TND",
                'data' => $currentAnnonce['prix'],

            ])
            ->add('musique', CheckBoxType::class, ['required' => false, 'data' => $currentAnnonce['musique'],
            ])
            ->add('pause', CheckBoxType::class, ['required' => false, 'data' => $currentAnnonce['pause'],
            ])
            ->add('bagage', CheckBoxType::class, ['required' => false, 'data' => $currentAnnonce['bagage'],
            ])
            ->add('createdby', HiddenType::class, [
                'data' => $user->getUsername(),
            ])
            ->add('submit', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $x = $form->getData();
            $x["datedepart"] = date_format($x["datedepart"], "yy-m-d");
            $x["heuredepart"] = date_format($x["heuredepart"], "H:i");
            $client = HttpClient::create();
            $response = $client->request('PUT', 'http://127.0.0.1:8001/modifierannonce/'.$annonceID, [
                'headers' => ["Content-Type" => "application/json"],
                'body' => json_encode($x),
            ]);
            return $this->redirectToRoute('accueil');

        }

        return $this->render(
            'annonce/modifierAnnonce.html.twig',
            array('form' => $form->createView())
        );
    }
}

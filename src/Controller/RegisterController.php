<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mime\Part\Multipart\FormDataPart;
use Symfony\Component\Routing\Annotation\Route;

class RegisterController extends AbstractController
{


    /**
     * @Route("/register", name="register")
     */
    public function register(Request $request)
    {

        $form = $this->createFormBuilder()

            ->add('email', EmailType::class)
            ->add('password', PasswordType::class)
            ->add('confirmpassword', PasswordType::class)
            ->add('role',CheckboxType::class, ['required' => false,])
            ->add('submit', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $client = HttpClient::create();
            $response = $client->request('POST', 'http://127.0.0.1:8001/inscription', [
                'headers' => ["Content-Type"=>"application/json"],
                'body' => json_encode($form->getData()),
            ]);
            return $this->redirectToRoute('accueil');
        }


        return $this->render(
            'register/registration.html.twig',
            array('form' => $form->createView())
        );

    }
}

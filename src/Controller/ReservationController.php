<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ReservationController extends AbstractController
{
    /**
     * @Route("/reservation/{reservationID}", name="reservation")
     */
    public function index(Request $request,$reservationID)
    {
        $form = $this->createFormBuilder()

            ->add('isComfirmed', CheckboxType::class,['required' => false])
            ->add('submit', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $x = $form->getData();
            $client = HttpClient::create();
            $response = $client->request('PUT', 'http://127.0.0.1:8001/modifierReservation/'.$reservationID, [
                'headers' => ["Content-Type" => "application/json"],
                'body' => json_encode($x),
            ]);
            return $this->redirectToRoute('accueil');

        }

        return $this->render(
            'reservation/index.html.twig',
            array('form' => $form->createView())
        );
    }
}

<?php

namespace App\Controller;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class VoitureController extends AbstractController
{
    /**
     * @Route("/voiture", name="voiture")
     * @param $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     *
     */
    public function voiture(Request $request)
    {
        $choicesModele = array();
        $choicesType = array();

        $client = HttpClient::create();
        $response = $client->request('GET', 'http://127.0.0.1:8001/allVoiture');
        foreach ($response->toArray() as $value) {

            array_push($choicesModele, [$value["modele"] => $value["id"]]);

        }

        $form = $this->createFormBuilder()
            ->add('matricule', TextType::class)
            ->add('modele', ChoiceType::class, ['choices' => $choicesModele])
            ->add('type', ChoiceType::class, [
                    'choices' =>
                        [
                            'select'=>'select'
                        ],
                ]

            )
            ->add('nbrplace', ChoiceType::class, ['choices' => [

                '1' => '1',
                '2' => '2',
                '3' => '3',
                '4' => '4',
            ],])
            ->add('climatisee', CheckboxType::class, ['required' => false,])
            ->add('submit', SubmitType::class)
            ->add('user', HiddenType::class, [
                'data' => $this->getUser()->getUsername(),
            ])
            ->getForm();
        $form->handleRequest($request);



        return $this->render(
            'voiture/voiture.html.twig',
            array('form' => $form->createView())
        );
    }


}

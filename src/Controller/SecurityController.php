<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="security_login")
     */
    public function index()
    {

        return $this->render('security/login.html.twig');

    }
    /**
     * @Route("/app_logout", name="security_logout")
     */
    public function logout()
    {

        return $this->render('accueil/accueil.html.twig');
    }
}

<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ProfilController extends AbstractController
{
    /**
     * @Route("/profil", name="profil")
     */
    public function profil(Request $request)
    {

        $form = $this->createFormBuilder()
            ->add('username', TextType::class)
            ->add('numtel', NumberType::class)
            ->add('fumeur', CheckboxType::class)
            ->add('genre', ChoiceType::class, array('choices' => array('Homme' => 'homme', 'Femme' => 'femme')))
            ->add('submit', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $client = HttpClient::create();
            $response = $client->request('POST', 'http://127.0.0.1:8001/completprofile/' . $user = $this->getUser()->getEmail(), [
                'headers' => ["Content-Type" => "application/json"],
                'body' => json_encode($form->getData()),
            ]);

        }
        return $this->render(
            'profil/profil.html.twig',
            array('form' => $form->createView())
        );
    }

    /**
     * @Route("/historiquepublication", name="historiquepublication")
     */
    public function historiquepublication(Request $request)
    {
        $client = HttpClient::create();
        $response = $client->request('GET', 'http://127.0.0.1:8001/getAnnonceCreatedBy/' . $user = $this->getUser()->getId());
        return $this->render(
            'profil/historiquepublication.html.twig'
            , ["data" => $response->toArray()]

        );
    }
    /**
     * @Route("/avis", name="avis")
     */
    public function avis(Request $request)
    {
        $client = HttpClient::create();
        $response = $client->request('GET', 'http://127.0.0.1:8001/getAnnonceCreatedBy/' . $user = $this->getUser()->getId());
        $responseUser = $client->request('GET', 'http://127.0.0.1:8001/getAvisByUser/' . $user = $this->getUser()->getId());

        return $this->render(
            'profil/avis.html.twig'
            , ["data" => $response->toArray(),"dataUser" => $responseUser->toArray()]

        );
    }

    /**
     * @Route("/reservations", name="reservations")
     */
    public function reservations(Request $request)
    {
        $form = $this->createFormBuilder()
            ->add('description', TextType::class)
            ->add('annonce', HiddenType::class)
            ->add('submit', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        $client = HttpClient::create();
        $response = $client->request('GET', 'http://127.0.0.1:8001/getReservationByUser/' . $user = $this->getUser()->getId());

        return $this->render(
            'profil/reservation.html.twig', ["data" => $response->toArray(),
                'form' => $form->createView()
            ]

        );
    }
}

<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mime\Part\DataPart;
use Symfony\Component\Mime\Part\Multipart\FormDataPart;
use Symfony\Component\Routing\Annotation\Route;

class AccueilController extends AbstractController
{
    /**
     * @Route("/", name="accueil")
     */
    public function index(Request $request)
    {


        $geo = file_get_contents('https://api.mapbox.com/geocoding/v5/mapbox.places/' . urlencode("tunisia") . '.json?access_token=pk.eyJ1IjoibWFsZWs5OCIsImEiOiJjanNnbnhubjMwMXE5NDNuem9sbjJ1NG9xIn0.S9QvJekjC8Hyn_AtUyw2uw');
        $geo = json_decode($geo, true); // Convert the JSON to an array

        $latitude = $geo['features'][0]['geometry']['coordinates']['0']; // Latitude
        $longitude = $geo['features'][0]['geometry']['coordinates']['1']; // Longitude
        $add = $geo['features'][0]['place_name'];


        $form = $this->createFormBuilder()
            ->add('villedepart', ChoiceType::class, ['choices' => [

                'Tunis' => 'Tunis',
                'Sousse' => 'Sousse',
                'Nabeul' => 'Nabeul',
                'Sfax' => 'Sfax',
                'Monastir' => 'Monastir',
                'Mahdia' => 'Mahdia',
                ' Kairouan' => ' Kairouan',
                'Zaghouan' => 'Zaghouan',
                ' Bizerte' => ' Bizerte',
                'Ben Arous' => 'Ben Arous',
                'Manouba' => 'Manouba',
                'Ariana' => 'Ariana',
                'Gabès' => 'Gabès',
                'Médenine' => 'Médenine',
                'Tataouine' => 'Tataouine',
                ' Kébili' => ' Kébili',
                'Tozeur' => 'Tozeur',
                ' Gafsa' => ' Gafsa',
                'Sidi Bouzid' => 'Sidi Bouzid',
                'Kasserine' => 'Kasserine',
                'Siliana' => 'Siliana',
                'Kef' => 'Kef',
                'Jendouba' => 'Jendouba',
                ' Béja' => ' Béja',

            ],])
            ->add('villearrivee', ChoiceType::class, ['choices' => [

                'Sousse' => 'Sousse',
                'Nabeul' => 'Nabeul',
                'Sfax' => 'Sfax',
                'Monastir' => 'Monastir',
                'Mahdia' => 'Mahdia',
                ' Kairouan' => ' Kairouan',
                'Zaghouan' => 'Zaghouan',
                ' Bizerte' => ' Bizerte',
                'Ben Arous' => 'Ben Arous',
                'Manouba' => 'Manouba',
                'Ariana' => 'Ariana',
                'Tunis' => 'Tunis',
                'Gabès' => 'Gabès',
                'Médenine' => 'Médenine',
                'Tataouine' => 'Tataouine',
                ' Kébili' => ' Kébili',
                'Tozeur' => 'Tozeur',
                ' Gafsa' => ' Gafsa',
                'Sidi Bouzid' => 'Sidi Bouzid',
                'Kasserine' => 'Kasserine',
                'Siliana' => 'Siliana',
                'Kef' => 'Kef',
                'Jendouba' => 'Jendouba',
                ' Béja' => ' Béja',

            ],])
            ->add('datedepart', DateType::class, [
                'widget' => 'single_text',
                'data' => new \DateTime(),
                'format' => 'yyyy-MM-dd',
                'attr' => ['class' => 'js-datepicker'],])
            ->add('heuredepart', TimeType::class, [
                'input' => 'datetime',
                'widget' => 'single_text'
            ])
            ->add('submit', SubmitType::class)
            ->getForm();
        $form->handleRequest($request);


        return $this->render('accueil/accueil.html.twig', array('form' => $form->createView(), 'latitude' => $latitude,
                'longitude' => $longitude,
                'adds' => $add,
                )
        );
    }




}

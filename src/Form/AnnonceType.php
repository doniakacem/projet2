<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AnnonceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('villedepart', ChoiceType::class, ['choices' => [

                'Tunis' => 'Tunis',
                'Sousse' => 'Sousse',
                'Nabeul' => 'Nabeul',
                'Sfax' => 'Sfax',
                'Monastir' => 'Monastir',
                'Mahdia' => 'Mahdia',
                ' Kairouan' => ' Kairouan',
                'Zaghouan' => 'Zaghouan',
                ' Bizerte' => ' Bizerte',
                'Ben Arous' => 'Ben Arous',
                'Manouba' => 'Manouba',
                'Ariana' => 'Ariana',
                'Gabès' => 'Gabès',
                'Médenine' => 'Médenine',
                'Tataouine' => 'Tataouine',
                ' Kébili' => ' Kébili',
                'Tozeur' => 'Tozeur',
                ' Gafsa' => ' Gafsa',
                'Sidi Bouzid' => 'Sidi Bouzid',
                'Kasserine' => 'Kasserine',
                'Siliana' => 'Siliana',
                'Kef' => 'Kef',
                'Jendouba' => 'Jendouba',
                ' Béja' => ' Béja',

            ],])
            ->add('villearrivee', ChoiceType::class, ['choices' => [

                'Sousse' => 'Sousse',
                'Nabeul' => 'Nabeul',
                'Sfax' => 'Sfax',
                'Monastir' => 'Monastir',
                'Mahdia' => 'Mahdia',
                ' Kairouan' => ' Kairouan',
                'Zaghouan' => 'Zaghouan',
                ' Bizerte' => ' Bizerte',
                'Ben Arous' => 'Ben Arous',
                'Manouba' => 'Manouba',
                'Ariana' => 'Ariana',
                'Tunis' => 'Tunis',
                'Gabès' => 'Gabès',
                'Médenine' => 'Médenine',
                'Tataouine' => 'Tataouine',
                ' Kébili' => ' Kébili',
                'Tozeur' => 'Tozeur',
                ' Gafsa' => ' Gafsa',
                'Sidi Bouzid' => 'Sidi Bouzid',
                'Kasserine' => 'Kasserine',
                'Siliana' => 'Siliana',
                'Kef' => 'Kef',
                'Jendouba' => 'Jendouba',
                ' Béja' => ' Béja',

            ],])
            ->add('pointarrivee', TextType::class)
            ->add('pointdepart', TextType::class)
            ->add('datedepart', DateType::class, [
                'widget' => 'single_text',
                'attr' => ['class' => 'js-datepicker'],
                'data' => new \DateTime(),

            ])
            ->add('heuredepart', TimeType::class)
            ->add('prix', TextType::class)
            ->add('musique', CheckBoxType::class, ['required' => false,])
            ->add('pause', CheckBoxType::class, ['required' => false,])
            ->add('bagage', CheckBoxType::class, ['required' => false,])
            ->add('submit', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => false
        ]);
    }
}
